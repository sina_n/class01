﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Scrip.Controller
{
	public class Character : MonoBehaviour
	{
		[SerializeField] private float speed;
		[SerializeField] private float jump;

		private void Update()
		{
			// transform.position += new Vector3(1, 0, 0) * (speed * Time.deltaTime);
		}

		public void Move()
		{
			transform.position += new Vector3(1, 0, 0) * (speed * Time.deltaTime);
		}

		public void Jump()
		{
			StartCoroutine(nameof(JumpCoroutine));
		}

		private IEnumerator JumpCoroutine()
		{
			var t = 0f;
			while (t < 1f)
			{
				t += Time.deltaTime;
				transform.position += Vector3.up * jump * Time.deltaTime;
				yield return null;
			}

			while (t < 2f)
			{
				t += Time.deltaTime;
				transform.position += Vector3.down * jump * Time.deltaTime;
				yield return null;
			}
		}
	}
}
