﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	[SerializeField] private float rotationSpeed = 30f;
	[SerializeField] private Vector3 defaultScale;

	private bool _triggerEntered = false;
	private bool _collected = false;
	
	private void Update()
	{
		transform.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
		if (_triggerEntered && !_collected)
		{
			StartCoroutine(nameof(CollectRoutine));
			_triggerEntered = false;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		_triggerEntered = true;
	}

	private IEnumerator CollectRoutine()
	{
		var t = 0f;
		_collected = true;
		rotationSpeed = 360;
		while (t < 2f)
		{
			t += Time.deltaTime;
			transform.position += Vector3.up * 5f * Time.deltaTime;
			transform.localScale = defaultScale * (2f-t) / 2f;
			yield return null;
		}
		Destroy(gameObject);
	}
	
}
