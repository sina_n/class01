﻿using System;
using System.Collections;
using _Scrip.Utils;
using UnityEngine;

namespace _Scrip.Controller
{
	public class GameManager : MonoBehaviourSingleton<GameManager>
	{

		[SerializeField] private Character character;
		private void Update()
		{
			if (Input.GetKey(KeyCode.W))
			{
				character.Move();
			}
			
			if (Input.GetKeyDown(KeyCode.Space))
			{
				character.Jump();
			}
		}
	}
}
